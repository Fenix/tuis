#! /bin/bash
#
# rationale: TUI app force
# version: 0.1 
#
# resources : https://github.com/charmbracelet/gum
#
export GUM_INPUT_CURSOR_FOREGROUND="#FF0"
export GUM_INPUT_PROMPT_FOREGROUND="#0FF"
# export GUM_INPUT_PLACEHOLDER="What's up?"
# export GUM_INPUT_PROMPT="* "
export GUM_INPUT_WIDTH=80

# Use fuzzy matching to filter a list of values:


echo Strawberry >> flavors.txt
echo Banana >> flavors.txt
echo Cherry >> flavors.txt
cat flavors.txt | gum filter > selection.txt
